var makeListOrderable = function (element) {

	var dragSourceElement;

	var handleDragStart = function (event) {

		// Target (this) element is the source node.
		dragSourceElement = event.target;
		event.dataTransfer.effectAllowed = "move";

		//event.dataTransfer.setData("text/html", this.innerHTML);
	};

	var handleDragOver = function (event) {

		if (event.preventDefault !== undefined) {
			event.preventDefault(); // Necessary. Allows us to drop.
		}

		// See the section on the DataTransfer object.
		event.dataTransfer.dropEffect = "move";

		return false;

	};

	var handleDragEnter = function (event) {
		// e.target is the current hover target.
		event.target.classList.add("over");

	};

	var handleDragLeave = function (event) {
		// e.target is previous target element.
		event.target.classList.remove("over");
	};

	var handleDrop = function (event) {

		// this / e.target is current target element.
		if (event.stopPropagation !== undefined) {
			event.stopPropagation(); // stops the browser from redirecting.
		}

		if (dragSourceElement !== event.target) {

			if (event.target.nextElementSibling === null) {
				event.target.parentNode.appendChild(dragSourceElement);
			} else if (dragSourceElement.previousElementSibling === null) {
				event.target.parentNode.insertBefore(event.target,dragSourceElement);
			} else {
				event.target.parentNode.insertBefore(dragSourceElement,event.target);
			}
		}

		// See the section on the DataTransfer object.

		return false;
	};

	var handleDragEnd = function (event) {

		// this/event.target is the source node.
		var draggables = event.currentTarget.querySelectorAll("*[draggable]")

		for (var index = 0, length = draggables.length; index < length; index++) {
			draggables[index].classList.remove("over");
		}

	};

	element.addEventListener("dragstart",handleDragStart,false);
	element.addEventListener("dragover",handleDragOver,false);
	element.addEventListener("dragenter",handleDragEnter,false);
	element.addEventListener("dragleave",handleDragLeave,false);
	element.addEventListener("drop",handleDrop,false);
	element.addEventListener("dragend",handleDragEnd,false);

};
