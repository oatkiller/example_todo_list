var tastListView = {

	localStorageKey : "tastList",

	markDoneButtonPrototype : (function () {

		var element = document.createElement("a");
		element.setAttribute("href","#");
		element.setAttribute("data-action","markDone");
		element.innerText = "[done]";
		return element;

	})(),

	deleteButtonPrototype : (function () {

		var element = document.createElement("a");
		element.setAttribute("href","#");
		element.setAttribute("data-action","delete");
		element.innerText = "[delete]";
		return element;

	})(),

	init : function (element) {

		this.element = element;

		// Bind event handlers
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleCompleteClick = this.handleCompleteClick.bind(this);
		this.handleIncompleteClick = this.handleIncompleteClick.bind(this);
		this.handleUnload = this.handleUnload.bind(this);

		// get references to certain elements
		// yes Devin, it would be better to use event delegation, but I'm not using jQuery
		this.incompleteTaskList = element.querySelector(".taskBag ol[data-role=\"incomplete\"]");
		this.completeTaskList = element.querySelector("ol[data-role=\"complete\"]");
		this.form = element.querySelector("form");

		// add event handlers
		this.form.addEventListener("submit",this.handleSubmit,false);
		this.incompleteTaskList.addEventListener("click",this.handleIncompleteClick,false);
		this.completeTaskList.addEventListener("click",this.handleCompleteClick,false);
		window.addEventListener("unload",this.handleUnload,false);

		// make the incompleteTaskList drag and drop orderable
		makeListOrderable(this.incompleteTaskList);

		this.hydrateStoredItems();

	},

	// handle clicks on the items in the complete list
	handleCompleteClick : function () {

		var target = event.target;

		// would be nice to let jQuery's event delegation stuff do these checks and get references to the elements that I am interested in
		if (target.nodeType === 1 && target.tagName === "A") {

			event.preventDefault();
			
			var action = target.getAttribute("data-action");

			if (action !== null) {

				var nearestTask = this.getNearestTaskElement(target);

				if (nearestTask !== undefined) {

					if (action === "delete") {
						this.removeTaskElement(nearestTask);
					}

				}

			}

		}

	},

	handleIncompleteClick : function (event) {

		var target = event.target;

		if (target.nodeType === 1 && target.tagName === "A") {

			event.preventDefault();
			
			var action = target.getAttribute("data-action");

			if (action !== null) {

				var nearestTask = this.getNearestTaskElement(target);

				if (nearestTask !== undefined) {

					if (action === "markDone") {

						this.markTaskElementDone(nearestTask);

					} else if (action === "delete") {

						this.removeTaskElement(nearestTask);
					}

				}

			}

		}

	},

	// handle the form submit
	handleSubmit : function (event) {

		event.preventDefault();
		
		var inputElement = this.form.querySelector("input");

		var value = inputElement.value;

		if (value !== "") {
			this.addItem(value);
			inputElement.value = "";
		}

	},

	handleUnload : function (event) {
		var data = this.getSerializedData();
		localStorage.setItem(this.localStorageKey,data);
	},

	hydrateStoredItems : function () {
		
		var serializedData = localStorage.getItem(this.localStorageKey);

		var data = JSON.parse(serializedData);

		var completeItems = data.completeItems;
		var incompleteItems = data.incompleteItems;

		incompleteItems.reverse().forEach(function (text) {
			this.addItem(text);
		},this);

		completeItems.reverse().forEach(function (text) {
			this.addCompleteItem(text);
		},this);

	},

	// get the nearest ancestor of target that is a task element
	getNearestTaskElement : function (target) {

		var nearestTask;
		var node = target.parentNode;

		while (node.getAttribute("data-type") !== "task" && node !== document.body) {
			node = node.parentNode;
		}

		if (node !== document.body) {
			return node;
		}

	},

	markTaskElementDone : function (target) {

		// you can't rearrange these elements
		target.removeAttribute("draggable");

		// remove the done button too
		var doneButton = target.querySelector("*[data-action=\"markDone\"]");
		doneButton.remove();

		// move it to the complete list
		this.completeTaskList.insertBefore(target,this.completeTaskList.firstChild);
		
	},

	removeTaskElement : function (target) {

		if (confirm("Delete this?") === true) {
			target.parentNode.removeChild(target);
		}

	},

	addItem : function (text) {

		var newListElement = this.createListElement(text);
		this.incompleteTaskList.insertBefore(newListElement,this.incompleteTaskList.firstChild);

	},

	addCompleteItem : function (text) {
		var newListElement = this.createListElement(text);
		this.markTaskElementDone(newListElement);
	},

	// create a fresh list element
	createListElement : function (text) {

		var element = document.createElement("li");
		element.setAttribute("data-type","task");
		element.setAttribute("draggable","true");
		element.innerText = text + " ";

		element.setAttribute("data-value",text);

		var markDoneButton = this.markDoneButtonPrototype.cloneNode(true);
		element.appendChild(markDoneButton);

		var aSpace = document.createTextNode(" ");
		element.appendChild(aSpace);

		var deleteButton = this.deleteButtonPrototype.cloneNode(true);
		element.appendChild(deleteButton);

		return element;
	},

	getSerializedData : function () {

		var incompleteItems = [];
		var completeItems = [];

		var data = {
			incompleteItems : incompleteItems,
			completeItems : completeItems
		};

		var incompleteItemElements = this.incompleteTaskList.querySelectorAll("li[data-type=\"task\"]");

		for (var index = 0, length = incompleteItemElements.length; index < length; index++) {
			incompleteItems.push(incompleteItemElements[index].getAttribute("data-value"));
		}

		var completeItemElements = this.completeTaskList.querySelectorAll("li[data-type=\"task\"]");

		for (var index = 0, length = completeItemElements.length; index < length; index++) {
			completeItems.push(completeItemElements[index].getAttribute("data-value"));
		}

		return JSON.stringify(data);

	}

};
